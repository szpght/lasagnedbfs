use bincode_derive::{Decode, Encode};
use im::OrdMap;
use std::rc::Rc;

type DirectoryContent = Box<OrdMap<Box<str>, u32>>;

#[repr(packed)]
#[derive(Debug, Copy, Clone)]
pub struct Pointer {
    pub device_id: u32,
    pub offset: u64,
    pub size: u32,
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum FsObjectType {
    File([Option<Pointer>; 4]),
    Directory(DirectoryContent),
    Device(u32),
    Symlink(String),
    Socket,
    Pipe,
}

#[repr(packed)]
#[derive(Debug, Clone)]
pub struct FsObject {
    pub id: u32,
    pub size: u64,
    pub object_type: FsObjectType,
    pub unix_permissions: u16,
    pub uid: u32,
    pub gid: u32,
    pub link_count: u32,
}
