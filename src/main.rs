use crate::in_memory::{FsObject, FsObjectType, Pointer};
use im::{HashMap, OrdMap};

mod in_memory;
mod on_disk;

fn main() {
    in_memory_something_scratch();
}

fn in_memory_something_scratch() {
    let filesystem_state = HashMap::new();
    let empty_file = FsObject {
        id: 2,
        size: 0,
        uid: 0,
        gid: 0,
        link_count: 1,
        unix_permissions: 0x644,
        object_type: FsObjectType::File([None, None, None, None]),
    };

    let root_directory = FsObject {
        id: 1,
        size: 0,
        uid: 0,
        gid: 0,
        unix_permissions: 0o755,
        link_count: 1,
        object_type: FsObjectType::Directory(Box::new(
            OrdMap::new().update(Box::from("empty_file"), empty_file.id),
        )),
    };

    let mut filesystem_state1 = filesystem_state.update(empty_file.id, empty_file);
    let mut filesystem_state2 = filesystem_state1.update(root_directory.id, root_directory);

    {
        let mut file_from_map = filesystem_state2.get_mut(&2).unwrap();
        file_from_map.link_count = 2;
    }

    let file_from_older_map = filesystem_state1.get(&2).unwrap();
    let file_from_newer_map = filesystem_state2.get(&2).unwrap();

    println!("{:?}", file_from_older_map.link_count);
    println!("{:?}", file_from_newer_map.link_count);
}
