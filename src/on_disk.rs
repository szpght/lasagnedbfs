use std::num::NonZeroU64;
use uuid::Uuid;
use zerocopy::{AsBytes, FromBytes};

const MAGIC: u64 = 0x1ACA4EFC;

#[repr(packed)]
#[derive(AsBytes, FromBytes, Clone, Copy, Debug, PartialEq, Default)]
pub struct Checksum([u8; 32]);

#[derive(AsBytes, FromBytes, Debug, Default)]
#[repr(packed)]
pub struct Superblock {
    magic: u64,
    pool_id: Uuid,
    device_id: u64,
    size: u64,
    metadata1_offset: u64,
    metadata1_length: u64,
    metadata2_offset: u64,
    metadata2_length: u64,

    checksum: Checksum,
}
